<h1>Hello World from PageController</h1>
<h2>{{ $front_end }}</h2> 

@if(count($topics) > 0)
    @foreach($topics as $topic)
        <li>{{ $topic }}</li>
    @endforeach
@else
    <p>Nothing to display</p>
@endif